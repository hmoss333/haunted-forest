﻿using UnityEngine;
using System;
using System.Collections;

public class Player_Move : MonoBehaviour
{

    public float speed;

    bool canMove = true;

    public float waitTime;
    public float attackTime;
    public float deathTime;

    public GameObject weapon;
    Vector2 attackDir;

    public static GameObject Instance;
    System_Pause systemPause;
    GameManager gameManager;
    System_Health systemHealth;

    public bool isDead;

    public PLAYER_STATE states;
    public MOVE_STATE moveStates;

    // Use this for initialization
    void Awake()
    {
        //waitTime = GameObject.FindGameObjectWithTag("System").GetComponent<GameManager>().levelStartDelay;
        //gridMaxX = GameObject.FindGameObjectWithTag("System").GetComponent<BoardManager>().columns;
        //gridMaxY = GameObject.FindGameObjectWithTag("System").GetComponent<BoardManager>().rows;
        systemPause = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<System_Pause>();
        //gameManager = GameObject.FindGameObjectWithTag("System").GetComponent<GameManager>();

        states = PLAYER_STATE.MOVE;
        moveStates = MOVE_STATE.IDLE;
    }

    void Start()
    {
        //waitTime = GameObject.FindGameObjectWithTag("System").GetComponent<GameManager>().levelStartDelay;

        if (Instance)
        {
            DestroyImmediate(this.gameObject);
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);
            Instance = this.gameObject;
        }

        StartCoroutine(PlayerFSM());
    }

    void FixedUpdate ()
    {
        systemHealth = this.GetComponent<System_Health>();

        //isPaused = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<System_Pause>().isPaused;
        //gridMaxX = GameObject.FindGameObjectWithTag("System").GetComponent<BoardManager>().columns;
        //gridMaxY = GameObject.FindGameObjectWithTag("System").GetComponent<BoardManager>().rows;

        //if (states == PLAYER_STATE.WAIT)
        //{
        //    //check if player inside board area;
        //    if (this.transform.position.x < 0 || this.transform.position.y < 0 || this.transform.position.x > gridMaxX || this.transform.position.y > gridMaxY)
        //    {
        //        this.transform.position = GameObject.FindGameObjectWithTag("System").GetComponent<BoardManager>().RandomPosition();
        //    }
        //}

        if (systemHealth.health <= 0)
        {
            //gameManager.GameOver();
            states = PLAYER_STATE.DIE;
        }

        if (states == PLAYER_STATE.MOVE)
        {
            if (canMove && !systemPause.isPaused && !isDead)
            {
                Vector3 movement = new Vector2();
                //Rigidbody rb = GetComponent<Rigidbody>();

                if (Input.GetAxisRaw("Vertical") > 0)
                {
                    moveStates = MOVE_STATE.UP;
                    movement.y += speed;
                }

                if (Input.GetAxisRaw("Vertical") < 0)
                {
                    moveStates = MOVE_STATE.DOWN;
                    movement.y -= speed;
                }
                if (Input.GetAxisRaw("Horizontal") < 0)
                {
                    moveStates = MOVE_STATE.LEFT;
                    movement.x -= speed;
                }
                if (Input.GetAxisRaw("Horizontal") > 0)
                {
                    moveStates = MOVE_STATE.RIGHT;
                    movement.x += speed;
                }
                if (Input.GetAxisRaw("Horizontal") == 0 && Input.GetAxisRaw("Vertical") == 0)
                {
                    movement = new Vector2(0, 0);
                }

                transform.position += movement * Time.deltaTime;
            }
        }
    }

    IEnumerator PlayerFSM()
    {
        while (true)
        {
            yield return StartCoroutine(states.ToString());
        }
    }

    IEnumerator WAIT()
    {
        yield return new WaitForSeconds(0);
        //Delay(waitTime * 1000);
        //states = PLAYER_STATE.MOVE;
    }

    IEnumerator MOVE()
    {
        yield return new WaitForSeconds(0);


        //Attack Logic
        if (Player_Weapon.instance == null && !systemPause.isPaused && !isDead)
        {
            if (Input.GetAxisRaw("Vertical_Attack") > 0)
            {
                attackDir = new Vector2(0, 1);
                states = PLAYER_STATE.ATTACK;
            }

            if (Input.GetAxisRaw("Vertical_Attack") < 0)
            {
                attackDir = new Vector2(0, -1);
                states = PLAYER_STATE.ATTACK;
            }
            if (Input.GetAxisRaw("Horizontal_Attack") > 0)
            {
                attackDir = new Vector2(1, 0);
                states = PLAYER_STATE.ATTACK;
            }

            if (Input.GetAxisRaw("Horizontal_Attack") < 0)
            {
                attackDir = new Vector2(-1, 0);
                states = PLAYER_STATE.ATTACK;
            }
        }
        //}
    }

    IEnumerator ATTACK()
    {
        //canMove = false;
        //play sound
        //play animation
        Instantiate(weapon, new Vector2(transform.position.x + attackDir.x / 2, transform.position.y + attackDir.y / 2), Quaternion.identity);
        yield return new WaitForSeconds(attackTime); //wait for seconds
        //Delay(attackTime * 1000);
        states = PLAYER_STATE.MOVE;
    }

    public IEnumerator DIE()
    {
        //play sound
        //play animtion
        canMove = false;
        isDead = true;
        yield return new WaitForSeconds(deathTime);
    }

    IEnumerator PAUSE()
    {
        yield return new WaitForSeconds(0);
    }


    public enum PLAYER_STATE
    {
        WAIT = 0,
        MOVE = 1,
        ATTACK = 2,
        DIE = 3,
        PAUSE = 4
    }

    public enum MOVE_STATE
    {
        UP = 0,
        DOWN = 1,
        LEFT = 2,
        RIGHT = 3,
        IDLE = 4
    }

    public void ChangeState (string toState)
    {
        StartCoroutine(DIE());
    }
}
