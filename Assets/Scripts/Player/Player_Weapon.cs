﻿using UnityEngine;
using System.Collections;

public class Player_Weapon : MonoBehaviour {

    public int damage;
    //public float pushForce;
    public float lifeTime;

    public float waitTime;
    public float damageTime;

    GameObject player;

    public WEAPON_STATE states;

    public static Player_Weapon instance = null;



    // Use this for initialization
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        player = GameObject.FindGameObjectWithTag("Player");

        Vector3 vectorToTarget = player.transform.position - transform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, q, 1);
        transform.parent = player.transform;

        states = WEAPON_STATE.MOVE;
    }

    void Start()
    {
        StartCoroutine(WeaponFSM());
    }

    IEnumerator WeaponFSM()
    {
        while (true)
        {
            yield return StartCoroutine(states.ToString());
        }
    }

    IEnumerator MOVE()
    {
        yield return new WaitForSeconds(waitTime);
        if (lifeTime >= 0)
        {
            lifeTime -= Time.deltaTime;
        }
        else
            states = WEAPON_STATE.DIE;
    }

    IEnumerator DAMAGE ()
    {
        yield return new WaitForSeconds(damageTime);
        states = WEAPON_STATE.MOVE;
    }

    IEnumerator DIE()
    {
        yield return new WaitForSeconds(0);
        Destroy(this.gameObject);
    }

    public enum WEAPON_STATE
    {
        MOVE = 0,
        DAMAGE = 1,
        DIE = 2
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            //push enemy back
            col.gameObject.transform.position = new Vector2(col.gameObject.transform.position.x + damage * Time.deltaTime, col.gameObject.transform.position.y + damage * Time.deltaTime);
            col.gameObject.GetComponent<System_Health>().SendMessage("Damage", damage);
            //col.gameObject.GetComponent<System_Health>().SendMessage("knockBack", pushForce);
            Debug.Log("Hit");

            states = WEAPON_STATE.DAMAGE;
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            //push enemy back
            col.gameObject.transform.position = new Vector2(col.gameObject.transform.position.x + damage * Time.deltaTime, col.gameObject.transform.position.y + damage * Time.deltaTime);
            col.gameObject.GetComponent<System_Health>().SendMessage("Damage", damage);
            //col.gameObject.GetComponent<System_Health>().SendMessage("knockBack", pushForce);
            Debug.Log("Hit");

            states = WEAPON_STATE.DAMAGE;
        }
    }
}
