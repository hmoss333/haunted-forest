﻿using UnityEngine;
using System.Collections;

public class System_LevelExit : MonoBehaviour {

    public Renderer render;

    // Use this for initialization
    void Start ()
    {
        render.enabled = false;
    }

    void Update () {
        GameObject[] enemies;
        enemies = GameObject.FindGameObjectsWithTag("Enemy");

        if (enemies.Length == 0)
        {
            render.enabled = true;
        }

    }

    void OnTriggerEnter2D (Collider2D col)
    {
        if (render.enabled == true && col.gameObject.tag == "Player")
        {
            //reload level
            col.gameObject.GetComponent<Player_Move>().states = Player_Move.PLAYER_STATE.WAIT;
            Application.LoadLevel(1);
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (render.enabled == true && col.gameObject.tag == "Player")
        {
            //reload level
            col.gameObject.GetComponent<Player_Move>().states = Player_Move.PLAYER_STATE.WAIT;
            Application.LoadLevel(1);
        }
    }
}
