﻿using UnityEngine;
using System.Collections;

public class System_Health : MonoBehaviour {

    public int health;

    float deathTime;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (health <= 0)
        {
            health = 0;
            if (GetComponent<Player_Move>() != null)
            {
                GetComponent<Player_Move>().StartCoroutine("DIE");
            }
            else if (GetComponent<AI_Follow>() != null)
            {
                GetComponent<AI_Follow>().StartCoroutine("DIE");
            }
            else if (GetComponent<AI_Teleporter>() != null)
            {
                GetComponent<AI_Teleporter>().StartCoroutine("DIE");
            }
            else if (GetComponent<AI_Charger>() != null)
            {
                GetComponent<AI_Charger>().StartCoroutine("DIE");
            }
        }
    }

    public void Damage(int damage)
    {
        health -= damage;
    }

    public void Heal (int heal)
    {
        health += heal;
    }
}
