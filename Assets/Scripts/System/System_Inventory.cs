﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class System_Inventory : MonoBehaviour {

    public bool useItem;

    public bool flashLight;
    public bool radio;
    public bool crowBar;
    public bool gasMask;
    public bool boltCutters;
    public bool grappleHook;
    public bool machette;
    public bool handOfGlory;

    public int itemNum;

    public bool flashLightOn;
    public bool gasMaskOn;

    //public GameObject toolPrefab;

    //public GameObject[] toolPics;

    Player_Move playerScript;

    // Use this for initialization
	void Start () {
        playerScript = GetComponent<Player_Move>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
		if (playerScript.states == Player_Move.PLAYER_STATE.MOVE)
        {
            if (Input.GetAxisRaw("Fire") != 0)
            {
                if (!useItem)
                {
                    UseTool(itemNum, playerScript.moveStates);
                    useItem = true;
                }
            }
            if (Input.GetAxisRaw("Fire") == 0)
            {
                useItem = false;
            }
        }
	}

    void UseTool(int equipedItem, Player_Move.MOVE_STATE directionState)
    {
        switch (equipedItem)
        {
            case 0:
                //flashlight toggle
                if (flashLight)
                {
                    Debug.Log("use flashlight");
                    //turn on the flashlight child
                    //sets position/rotation around player based on directionState
                    if (!flashLightOn)
                        flashLightOn = true;
                    else
                        flashLightOn = false;
                }
                break;
            case 1:
                //radio event
                if (radio)
                {
                    //paus the player and play a message based on where the player is
                    playerScript.states = Player_Move.PLAYER_STATE.WAIT;
                    Debug.Log("radio event");
                    playerScript.states = Player_Move.PLAYER_STATE.MOVE;
                }
                break;
            case 2:
                //crowbar
                if (crowBar)
                {
                    //if equiped and in trigger volume, start ToolGame to remove obstacle
                    Debug.Log("use crowbar");
                    ToolGame();
                }
                break;
            case 3:
                //gas mask toggle
                if (gasMask)
                {
                    //turn on the gas mask child
                    //sets position/rotation around player based on directionState
                    Debug.Log("gas mask on");
                    if (!gasMaskOn)
                        gasMaskOn = true;
                    else
                        gasMaskOn = false;
                }
                break;
            case 4:
                //bolt cutters
                if (boltCutters)
                {
                    //if equiped and in trigger volume, start ToolGame to remove obstacle
                    Debug.Log("use bolt cutters");
                    ToolGame();
                }
                break;
            case 5:
                //grapple hook
                if (grappleHook)
                {
                    //hahaha I have no fucking clue yet
                    Debug.Log("use grapple hook");
                }
                break;
            case 6:
                //Machette
                if (machette)
                {
                    //turn on the machette child
                    //machette has a trigger volume, will destroy/damage obstacles
                    //set position/rotation around player based on directionState
                    Debug.Log("use machette");
                }
                break;
            case 7:
                //Hand of Glory
                if (handOfGlory)
                {
                    //open any door when in trigger volume
                    //only way to open final door
                    Debug.Log("use Hand of Glory");
                }
                break;

            default:
                //base logic
                //i.e. do nothing
                Debug.Log("no items equiped");
                break;
        }
    }

    public void ToolGame ()
    {
        playerScript.states = Player_Move.PLAYER_STATE.WAIT;
        //multi-tap mini-game
        //use for crowbar, bolt cutters and grapple
        playerScript.states = Player_Move.PLAYER_STATE.MOVE;
    }
}
