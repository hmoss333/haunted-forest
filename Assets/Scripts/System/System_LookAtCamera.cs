﻿using UnityEngine;
using System;
using System.Collections;

public class System_LookAtCamera : MonoBehaviour {

    public GameObject target;

	// Use this for initialization
	void Start () {
        target = GameObject.FindGameObjectWithTag("MainCamera");
    }
	
	// Update is called once per frame
	void Update () {
        // Project cameraForward onto the plane, to get our target.
        Vector3 yTarget = Camera.main.transform.up - (transform.forward * Vector3.Dot(Camera.main.transform.up, transform.forward));

        // Find the needed rotation to rotate y to y-target
        Quaternion desiredRotation = Quaternion.LookRotation(transform.forward, yTarget);

        // Apply that rotation
        transform.rotation = desiredRotation;
    }
}
