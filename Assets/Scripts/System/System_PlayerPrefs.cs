﻿using UnityEngine;
using System.Collections;

public class System_PlayerPrefs : MonoBehaviour {

    GameObject player;
    GameManager gameManager;
    
    // Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");

        DontDestroyOnLoad(this);
        //PlayerPrefs.SetInt("levelNum", gameManager.level);
        //PlayerPrefs.SetInt("playerHealth", player.GetComponent<System_Health>().health);
        //PlayerPrefs.SetString("playerWeapon", "empty");
	}
	
	// Update is called once per frame
	void Update () {
	    //write current level to player prefs
        //write player health to player prefs
        //write player weapon to player prefs
        //write board data to player prefs?
	}

    public void SetData(string key, int data)
    {
        if (PlayerPrefs.HasKey(key))
        {
            PlayerPrefs.SetInt(key, data);
        }

        PlayerPrefs.Save();
    }
}
