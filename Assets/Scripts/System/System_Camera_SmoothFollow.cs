﻿using UnityEngine;
using System.Collections;

public class System_Camera_SmoothFollow : MonoBehaviour {

    public float distance;
    public float dampTime = 0.15f;
    private Vector3 velocity = Vector3.zero;
    public GameObject target;

    // Update is called once per frame
    void Awake ()
    {
        //target = GameObject.FindGameObjectWithTag("Player");
    }

    void Start ()
    {
        target = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        if (target)
        {
            Vector3 point = GetComponent<Camera>().WorldToViewportPoint(target.transform.position);
            Vector3 delta = target.transform.position - GetComponent<Camera>().ViewportToWorldPoint(new Vector3(0.5f, 0.5f, distance)); //(new Vector3(0.5, 0.5, point.z));
            Vector3 destination = transform.position + delta;
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
            target.transform.rotation = this.transform.rotation;
        }

    }
}
