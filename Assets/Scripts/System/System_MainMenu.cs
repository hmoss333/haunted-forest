﻿using UnityEngine;
using System.Collections;

public class System_MainMenu : MonoBehaviour {

    public float buttonWidth;
    public float buttonHeight;

    GameObject player;
    GameObject[] gameManagers;

    // Use this for initialization
    void Start () {
        //Application.UnloadLevel(1);

        player = GameObject.FindGameObjectWithTag("Player");
        gameManagers = GameObject.FindGameObjectsWithTag("System");
    }
	
	// Update is called once per frame
	void Update () {
	    if (player != null)
        {
            Destroy(player);
        }
        if (gameManagers != null)
        {
            for (int i = 0; i < gameManagers.Length; i++)
            {
                Destroy(gameManagers[i]);
            }
        }
	}

    void OnGUI ()
    {
        GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "Main Menu");

        if (GUI.Button(new Rect((Screen.width - buttonWidth) / 2, (Screen.height - buttonHeight) / 4, buttonWidth, buttonHeight), "New Game"))
        {
            Application.LoadLevel(1);
        }

        if (GUI.Button(new Rect((Screen.width - buttonWidth) / 2, (Screen.height - buttonHeight) / 2, buttonWidth, buttonHeight), "Quit"))
        {
            Application.Quit();
        }
    }
}
