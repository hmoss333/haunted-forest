﻿using UnityEngine;
using System.Collections;

public class System_HealthGUI : MonoBehaviour {

    public GUIContent prefab;
    public int health;
    
    // Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        health = GameObject.FindGameObjectWithTag("Player").GetComponent<System_Health>().health;
    }

    void OnGUI ()
    {
        GUI.Box(new Rect(0, 0, prefab.image.width, prefab.image.height), prefab);
        GUI.color = Color.black;
        GUI.Label(new Rect(prefab.image.width / 2.5f, prefab.image.height/ 5, prefab.image.width, prefab.image.height), health.ToString());
    }
}
