﻿using UnityEngine;
using System.Collections;

public class System_BossItemDrop : MonoBehaviour {

    public GameObject[] weaponList;
    int bossHealth;
    int objectCount;

    // Use this for initialization
    void Start () {
        objectCount = Random.Range(0, weaponList.Length);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        bossHealth = this.GetComponent<System_Health>().health;

        if (bossHealth <= 0)
        {
            DropItem();
            Destroy(this);
        }
    }

    public void DropItem() //change Enemy timer so when state == DIE, call method
    {
        Instantiate(weaponList[objectCount], gameObject.transform.position, Quaternion.identity);
    }
}
