﻿using UnityEngine;
using System.Collections;

public class System_Pickups : MonoBehaviour {

    public int damage;
    public int health;
    public float attackRate;
    
    // Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            col.GetComponent<Player_Move>().weapon.GetComponent<Player_Weapon>().damage += damage;
            col.GetComponent<Player_Move>().weapon.GetComponent<Player_Weapon>().waitTime -= attackRate;
            col.GetComponent<Player_Move>().weapon.GetComponent<Player_Weapon>().damageTime -= attackRate;
            col.GetComponent<System_Health>().SendMessage("Heal", health);
            Debug.Log("Picked up");
            Destroy(this.gameObject);
        }

    }
}
