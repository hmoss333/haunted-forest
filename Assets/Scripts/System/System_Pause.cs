﻿using UnityEngine;
using System.Collections;

public class System_Pause : MonoBehaviour {

    public bool isPaused = false;
    public float inputDelay;

    public float buttonWidth = 100;
    public float buttonHeight = 50;

    public PAUSE_STATE states;

    Player_Move player;

    void Awake ()
    {
        states = PAUSE_STATE.PAUSE;
    }
    
    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Move>();

        StartCoroutine(PauseFSM());
    }

    IEnumerator PauseFSM()
    {
        while (true)
        {
            yield return StartCoroutine(states.ToString());
        }
    }

    IEnumerator PAUSE()
    {
        if (Input.GetAxisRaw("Pause") > 0 && GameObject.FindGameObjectWithTag("System").GetComponent<GameManager>().doingSetup == false)
        {
            if (isPaused == false)
            {
                Debug.Log("Pause");
                isPaused = true;
                Time.timeScale = 0;
                yield return new WaitForSeconds(inputDelay);
            }
            else if (isPaused == true)
            {
                Debug.Log("Unpause");
                isPaused = false;
                Time.timeScale = 1;
                yield return new WaitForSeconds(inputDelay);
            }
        }
    }

    public enum PAUSE_STATE
    {
        PAUSE = 0
    }

    //Turn on/off GUI
    void OnGUI()
    {
        if (isPaused && !player.isDead)
        {
            GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "Paused");

            if (GUI.Button(new Rect((Screen.width - buttonWidth) / 2, (Screen.height - buttonHeight) / 4, buttonWidth, buttonHeight), "Main Menu"))
            {
                Application.LoadLevel(0);
            }

            if (GUI.Button(new Rect((Screen.width - buttonWidth) / 2, (Screen.height - buttonHeight) / 2, buttonWidth, buttonHeight), "Return"))
            {
                isPaused = false;
            }
        }

        else if (player.isDead)
        {
            GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "Game Over");

            if (GUI.Button(new Rect((Screen.width - buttonWidth) / 2, (Screen.height - buttonHeight) / 4, buttonWidth, buttonHeight), "Main Menu"))
            {
                Application.LoadLevel(0);
            }
        }
    }
}
