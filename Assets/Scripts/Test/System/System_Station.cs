﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class System_Station : MonoBehaviour {

    float stationNum;
    float signalStrength;

    bool isActive;
    
    // Use this for initialization
	void Start () {
        isActive = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D (Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            isActive = true;
        }
    }

    void OnTriggerExit2D (Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            isActive = false;
        }
    }
}
