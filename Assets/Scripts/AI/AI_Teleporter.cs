﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic; 		//Allows us to use Lists.
using Random = UnityEngine.Random; 		//Tells Random to use the Unity Engine random number generator.

public class AI_Teleporter : MonoBehaviour
{
    public bool isBoss;
    bool canMove;
    public float idleTime;
    public float waitTime;
    public float attackTime;
    public float attackDelay;
    public float deathTime;

    float gridMaxX;
    float gridMaxY;

    public float minDist;
    public float checkDist;
    Vector2 targetPos;

    public int projectileCount;
    public GameObject prefab;
    bool hasTarget;

    GameObject player;
    System_Health systemHealth;
    System_BossItemDrop bossDrop;

    public ENEMY_STATE states;

    bool isPaused;
    //bool playerDead;

    //bool running;

    public void Awake()
    {
        //waitTime = GameObject.FindGameObjectWithTag("System").GetComponent<GameManager>().levelStartDelay;
        //gridMaxX = GameObject.FindGameObjectWithTag("System").GetComponent<BoardManager>().columns;
        //gridMaxY = GameObject.FindGameObjectWithTag("System").GetComponent<BoardManager>().rows;

        states = ENEMY_STATE.WAIT;

        //if (isBoss)
        //{
        //    bossDrop = GetComponent<System_BossItemDrop>();
        //}

        //if (player)
        //{
        //    hasTarget = true;
        //}

        //running = true;
    }

    public void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        waitTime = GameManager.instance.levelStartDelay;
        gridMaxX = GameObject.FindGameObjectWithTag("System").GetComponent<BoardManager>().columns;
        gridMaxY = GameObject.FindGameObjectWithTag("System").GetComponent<BoardManager>().rows;

        if (isBoss)
        {
            bossDrop = GetComponent<System_BossItemDrop>();
        }

        if (player)
        {
            hasTarget = true;
        }

        StartCoroutine(EnemyFSM());
    }

    void Update()
    {
        systemHealth = this.GetComponent<System_Health>();

        if (player == null)
        {
            player = this.gameObject;
            hasTarget = false;
        }

        isPaused = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<System_Pause>().isPaused;
        //playerDead = player.GetComponent<Player_Move>().isDead;

        if (systemHealth.health <= 0)
        {
            states = ENEMY_STATE.DIE;
        }
    }

    IEnumerator EnemyFSM()
    {
        while (true)
        {
            yield return StartCoroutine(states.ToString());
        }
    }

    IEnumerator WAIT()
    {
        yield return new WaitForSeconds(waitTime + 1);
        //Delay((waitTime + 1)* 1000);
        states = ENEMY_STATE.IDLE;
    }

    IEnumerator IDLE()
    {
        if (Vector2.Distance(transform.position, player.transform.position) < checkDist)
        {
            yield return new WaitForSeconds(idleTime);
            //StartCoroutine(WaitTimer(idleTime));
            //Delay(idleTime * 1000);
            states = ENEMY_STATE.MOVE;
        }
    }

    IEnumerator MOVE()
    {
        SEARCH();

        // ENTER THE MOVE STATE
        while (states == ENEMY_STATE.MOVE && !isPaused)// && !playerDead)
        {
            if (checkIfPosEmpty(targetPos))
            {
                yield return new WaitForSeconds(0.1f);
                //Delay(0.1f * 1000);
                transform.position = targetPos;

                states = ENEMY_STATE.ATTACK;
            }
            else
            {
                Debug.Log("oops");
                SEARCH();
            }
        }
    }

    IEnumerator ATTACK()
    {
        yield return new WaitForSeconds(attackDelay);

        while (states == ENEMY_STATE.ATTACK && !isPaused)// && !playerDead)
        {
            if (hasTarget)
            {
                for (int i = 0; i < projectileCount; i++)
                {
                    Instantiate(prefab, transform.position, Quaternion.identity);
                    yield return new WaitForSeconds(attackTime);
                    //Delay(attackTime * 1000);
                }
                states = ENEMY_STATE.IDLE;
            }
            else if (!hasTarget)
                states = ENEMY_STATE.IDLE;
        }
    }

    IEnumerator DIE()
    {
        yield return new WaitForSeconds(deathTime);
        //Delay(deathTime * 1000);
        if (isBoss)
        {
            bossDrop.SendMessage("DropItem");
        }
        Destroy(this.gameObject);
    }

    public enum ENEMY_STATE
    {
        WAIT = 0,
        IDLE = 1,
        MOVE = 2,
        ATTACK = 3,
        DIE = 4
    }

    public void SEARCH()
    {
        targetPos = new Vector2(Random.Range(player.transform.position.x + minDist, player.transform.position.x - minDist), Random.Range(player.transform.position.y + minDist, player.transform.position.y - minDist));
        if (targetPos.x < 0 || targetPos.y < 0 || targetPos.x > gridMaxX || targetPos.y > gridMaxY) //check min/max board size instead. replace later
        {
            SEARCH();
        }
        Debug.Log("checking");
    }

    public bool checkIfPosEmpty(Vector3 targetPos)
    {
        Collider2D[] allThings = GameObject.FindObjectsOfType<Collider2D>();

        foreach (Collider2D current in allThings)
        {
            if (current.GetComponent<Collider2D>().OverlapPoint(targetPos))
                return false;
        }
        return true;
    }

    private void Delay(float delay)
    {
        float t = Environment.TickCount;
        while ((Environment.TickCount - t) < delay) ;
    }
}
