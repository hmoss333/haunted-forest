﻿using UnityEngine;
using System;
using System.Collections;

public class AI_Follow : MonoBehaviour {

    public float speed;
    public int damage;

    public float checkDist;
    bool canMove = true;
    public float waitTime;

    public float attackTime;
    public float attackDist;

    public float deathTime;

    public bool isSpawn;

    public GameObject player;
    System_Health systemHealth;

    public ENEMY_STATE states;

    bool isPaused;
    //bool playerDead;

    public void Awake()
    {
        //if (!isSpawn)
        //{
        //    waitTime = GameObject.FindGameObjectWithTag("System").GetComponent<GameManager>().levelStartDelay;
        //}

        states = ENEMY_STATE.WAIT;
    }

    public void Start()
    {
        waitTime = GameManager.instance.levelStartDelay;
        player = GameObject.FindGameObjectWithTag("Player");

        StartCoroutine(EnemyFSM());
    }

    void Update ()
    {
        if (!systemHealth)
            systemHealth = this.GetComponent<System_Health>();

        if (systemHealth.health <= 0)
        {
            StopCoroutine("WaitTimer");
            states = ENEMY_STATE.DIE;
        }

        if (player == null)
        {
            player = this.gameObject;
            damage = 0;
        }

        isPaused = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<System_Pause>().isPaused;
        //if (player != this || player != null)
        //    playerDead = player.GetComponent<Player_Move>().isDead;
    }

    IEnumerator EnemyFSM()
    {
        while (true)
        {
            yield return StartCoroutine(states.ToString());
        }
    }

    IEnumerator WAIT()
    {
        if (!isSpawn)
        {
            yield return new WaitForSeconds(waitTime + 1);
            //Delay((waitTime + 1) * 1000);
        }

        states = ENEMY_STATE.IDLE;
    }

    IEnumerator IDLE()
    {
        if (Vector2.Distance(transform.position, player.transform.position) < checkDist)
        {
            yield return new WaitForSeconds(0);
            states = ENEMY_STATE.MOVE;
        }
    }

    IEnumerator MOVE()
    {
        if (canMove && !isPaused && !player.GetComponent<Player_Move>().isDead)
        {
            yield return new WaitForSeconds(0);
            transform.position = Vector2.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);

            if (Vector2.Distance(transform.position, player.transform.position) < attackDist)
            {
                states = ENEMY_STATE.ATTACK;
            }
            else if (Vector2.Distance(transform.position, player.transform.position) > checkDist)
            {
                states = ENEMY_STATE.IDLE;
            }
        }
    }

    IEnumerator ATTACK()
    {
        //play sound
        //play animation
        yield return new WaitForSeconds(attackTime); //wait for seconds
        //Delay(attackTime * 1000);
        if (Vector2.Distance(transform.position, player.transform.position) < attackDist)// && !playerDead)
        {
            //then (send message to Health on player)
            player.gameObject.GetComponent<System_Health>().SendMessage("Damage", damage);
        }
        states = ENEMY_STATE.IDLE;
    }

    IEnumerator DIE()
    {
        //play sound
        //play animtion
        yield return new WaitForSeconds(deathTime);
        //Delay(deathTime * 1000);
        Destroy(this.gameObject);
    }

    public enum ENEMY_STATE
    {
        WAIT = 0,
        IDLE = 1,
        MOVE = 2,
        ATTACK = 3,
        DIE = 4
    }

    private void Delay(float delay)
    {
        float t = Environment.TickCount;
        while ((Environment.TickCount - t) < delay) ;
    }
}
