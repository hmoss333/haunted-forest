﻿using UnityEngine;
using System;
using System.Collections;

public class AI_Charger : MonoBehaviour {

    bool canMove;

    public float speed;
    public int damage;

    public float checkDist;
    public float lookTime;

    public float waitTime;

    public float attackTime;
    public float attackDist;
    bool hasHurt;

    bool isHitting;

    public float deathTime;

    [SerializeField] GameObject player;
    Vector3 targetPosition;
    Vector3 currentPosition;
    Vector3 directionOfTravel;
    [SerializeField] System_Health systemHealth;

    public ENEMY_STATE states;

    [SerializeField] bool isPaused;
    //[SerializeField] bool playerDead;

    // Use this for initialization
    void Awake () {
        isHitting = false;

        states = ENEMY_STATE.WAIT;
    }

    void Start ()
    {
        waitTime = GameManager.instance.levelStartDelay; //GameObject.FindGameObjectWithTag("System").GetComponent<GameManager>().levelStartDelay;
        player = GameObject.FindGameObjectWithTag("Player");
        systemHealth = this.GetComponent<System_Health>();

        StartCoroutine(EnemyFSM());
    }
	
	// Update is called once per frame
	void Update () {
        if (!player)
            player = GameObject.FindGameObjectWithTag("Player");
        if (!systemHealth)
            systemHealth = GetComponent<System_Health>();

        if (systemHealth.health <= 0)
        {
            states = ENEMY_STATE.DIE;
        }

        if (player == null)
        {
            player = this.gameObject;
            damage = 0;
        }

        isPaused = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<System_Pause>().isPaused;
        //if (player != this || player != null)
        //    playerDead = player.GetComponent<Player_Move>().isDead;
    }

    IEnumerator EnemyFSM()
    {
        while (true)
        {
            yield return StartCoroutine(states.ToString());
        }
    }

    IEnumerator WAIT()
    {
        yield return new WaitForSeconds(waitTime + 1);
        //Delay((waitTime + 1) * 1000);

        states = ENEMY_STATE.IDLE;
    }

    IEnumerator IDLE()
    {
        //isHitting = false;
        transform.position = this.transform.position;

        if (Vector2.Distance(transform.position, player.transform.position) < checkDist)
        {
            isHitting = false;
            yield return new WaitForSeconds(0);
            states = ENEMY_STATE.LOOK;
        }
    }

    IEnumerator LOOK()
    {
        yield return new WaitForSeconds(lookTime);
        //Delay(lookTime * 1000);

        targetPosition = player.transform.position;
        currentPosition = this.transform.position;

        states = ENEMY_STATE.ATTACK;
    }

    IEnumerator ATTACK()
    {
        if (!isPaused)// && !playerDead)
        {
            //play sound
            //play animation
            yield return new WaitForSeconds(0); //wait for seconds

            directionOfTravel = targetPosition - currentPosition;
            //now normalize the direction, since we only want the direction information
            directionOfTravel.Normalize();
            //scale the movement on each axis by the directionOfTravel vector components
            this.transform.Translate(
                (directionOfTravel.x * speed * Time.deltaTime),
                (directionOfTravel.y * speed * Time.deltaTime),
                0.0f, Space.World);
            if (Vector2.Distance(transform.position, player.transform.position) < attackDist && hasHurt == false)// && !playerDead)
            {
                StartCoroutine(DamagePlayer());
            }
            if (isHitting == true)
            {
                states = ENEMY_STATE.IDLE;
            }
        }
    }

    IEnumerator DIE()
    {
        //play sound
        //play animtion
        yield return new WaitForSeconds(deathTime);
        Destroy(this.gameObject);
    }

    public enum ENEMY_STATE
    {
        WAIT = 0,
        IDLE = 1,
        LOOK = 2,
        ATTACK = 3,
        DIE = 4,
    }

    //public enum LAST_STATE
    //{
    //    WAIT = 0,
    //    IDLE = 1,
    //    LOOK = 2,
    //    ATTACK = 3,
    //    DIE = 4,
    //    PAUSE = 5
    //}

    void OnCollisionEnter2D (Collision2D col)
    {
        isHitting = true;
    }

    void OnCollisionEnter(Collision col)
    {
        isHitting = true;
    }

    IEnumerator DamagePlayer ()
    {
        hasHurt = true;
        player.gameObject.GetComponent<System_Health>().SendMessage("Damage", damage);
        yield return new WaitForSeconds(attackTime);
        //Delay(attackTime * 1000);
        hasHurt = false;
    }

    private void Delay(float delay)
    {
        float t = Environment.TickCount;
        while ((Environment.TickCount - t) < delay) ;
    }
}
