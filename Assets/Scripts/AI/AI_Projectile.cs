﻿using UnityEngine;
using System.Collections;

public class AI_Projectile : MonoBehaviour {

    public float speed;
    public int damage;
    Vector3 target;
    Transform tempTarget;

    //an array of props
    private GameObject[] player;
    private GameObject[] prop;

    public GameObject prefab;

    bool isPaused;

    // Use this for initialization
    void Awake()
    {
        //Find player in scene
        player = GameObject.FindGameObjectsWithTag("Player");
        tempTarget = player[UnityEngine.Random.Range(0, player.Length)].transform; //leave random in for future reference

        //Set player's coordinates at instantiation as target
        target.x = tempTarget.position.x;
        target.y = tempTarget.position.y;
        target.z = tempTarget.position.z;

        //Set rotation of Projectile towards player
        Vector3 vectorToTarget = target - transform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, q, speed);

        //Check is scene is Paused
        isPaused = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<System_Pause>().isPaused;
    }

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isPaused)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }

        //Move projectile in direction of target position indefinitely
        transform.position += transform.right * Time.deltaTime * speed;

        Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        if (screenPosition.y > Screen.height || screenPosition.y < 0)
        {
            Destroy(this.gameObject);
        }
        else if (screenPosition.x > Screen.width || screenPosition.x < 0)
        {
            Destroy(this.gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        //If colliding with a prop, create enemy & destroy self
        if (col.gameObject.tag == "Prop")
        {
            Instantiate(prefab, col.transform.position, col.transform.rotation);
            Destroy(col.gameObject);
        }

        if (col.gameObject.tag == "Player")
        {
            col.gameObject.GetComponent<System_Health>().SendMessage("Damage", damage);
        }
        Destroy(this.gameObject);
    }

    void OnCollisionEnter(Collision col)
    {
        //If colliding with a prop, create enemy & destroy self
        if (col.gameObject.tag == "Prop")
        {
            Instantiate(prefab, col.transform.position, prefab.transform.rotation);
            Destroy(col.gameObject);
        }

        if (col.gameObject.tag == "Player")
        {
            col.gameObject.GetComponent<System_Health>().SendMessage("Damage", damage);
        }
        Destroy(this.gameObject);
    }
}
